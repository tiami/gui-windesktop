﻿using System;
using System.Windows;
using System.Windows.Forms.VisualStyles;

namespace eksamen_gui19_anita
{
    public partial class ModelDialog : System.Windows.Window
    {
        public ModelDialog()
        {
            InitializeComponent();
            Close_Button.Click += CloseButton_Click;
        }

        public event EventHandler Apply;

        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            // Validate all controls
            //if (ValidateBindings(this))
            //{
            
            Name = TxtName.Text;
            Phone = int.Parse(TxtPhone.Text);
            Weight = int.Parse(TxtWeight.Text);
            Height = int.Parse(TxtHeight.Text);
            Comments = TxtComment.Text;
            Hair = TxtHair.Text;
            Apply?.Invoke(this, EventArgs.Empty);

            //}
        }

        public void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        ModelDialogData data = new ModelDialogData();

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            TxtName.Focus();
        }

        public string Name
        {
            get { return data.ModelName; }
            set { data.ModelName = value; }
        }

        public int Phone
        {
            get { return data.ModelPhone; }
            set { data.ModelPhone = value; }
        }
        public int Height
        {
            get { return data.ModelHeight; }
            set { data.ModelHeight = value; }
        }
        public int Weight
        {
            get { return data.ModelWeight; }
            set { data.ModelWeight = value; }
        }
        public string Hair
        {
            get { return data.ModelHair; }
            set { data.ModelHair = value; }
        }
        public string Comments
        {
            get { return data.ModelComment; }
            set { data.ModelComment = value; }
        }
    }
}