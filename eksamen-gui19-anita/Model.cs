﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eksamen_gui19_anita
{
    [Serializable]
    public class Model
    {
        public int ModelId { get; set; }
        public string ModelName { get; set; }
        public int ModelPhone { get; set; }
        public int ModelHeight { get; set; }
        public int ModelWeight { get; set; }
        public string ModelHair { get; set; }
        public string ModelComments { get; set; }

        public override string ToString()
        {
            return ModelId + ": " + ModelName + ", " + ModelWeight + "kg, " + ModelHeight + "cm, " + ModelHair + " hair. \n" + "Phone: " + ModelPhone + " Comment: " + ModelComments;
        }
    }
}
