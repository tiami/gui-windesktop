﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eksamen_gui19_anita
{
    /// <summary>
    /// Interaction logic for JobDialog.xaml
    /// </summary>
    public partial class JobDialog : System.Windows.Window
    {
        public JobDialog()
        {
            InitializeComponent();
            Close_Button.Click += CloseButton_Click;
        }
        public event EventHandler Apply;

        public void AddButton_Click(object sender, RoutedEventArgs e)
        {
            // Validate all controls
            //if (ValidateBindings(this))
            //{

            Customer = TxtCustomer.Text;
            Date = (DateTime) TxtDate.SelectedDate;
            Duration = int.Parse(TxtDuration.Text);
            Address = TxtAddress.Text;
            Models = int.Parse(TxtModels.Text);
            Comments = TxtComment.Text;
            Apply?.Invoke(this, EventArgs.Empty);

            //}
        }

        public void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        JobDialogData data = new JobDialogData();

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            TxtCustomer.Focus();
        }

        public string Customer
        {
            get { return data.Customer; }
            set { data.Customer = value; }
        }

        public DateTime Date
        {
            get { return data.Date; }
            set { data.Date = value; }
        }
        public int Duration
        {
            get { return data.Duration; }
            set { data.Duration = value; }
        }
        public string Address
        {
            get { return data.Address; }
            set { data.Address = value; }
        }
        public int Models
        {
            get { return data.Models; }
            set { data.Models = value; }
        }
        public string Comments
        {
            get { return data.Comments; }
            set { data.Comments = value; }
        }
    }
}
