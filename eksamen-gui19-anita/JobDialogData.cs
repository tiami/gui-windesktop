﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eksamen_gui19_anita
{
    public class JobDialogData
    {
        public string Customer { get; set; }
        public DateTime Date { get; set; }
        public int Duration { get; set; }
        public string Address { get; set; }
        public int Models { get; set; }
        public string Comments { get; set; }
    }
}
