﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eksamen_gui19_anita
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Job> _joblist { get; set; }
        public ObservableCollection<Model> _modellist { get; set; }
        public ObservableCollection<Job> _jobsAssigned { get; set; }
        private ModelDialog dlg;
        private JobDialog dg;

        public MainWindow()
        {
            InitializeComponent();
            FillJobsGrid();
            FillModelsGrid();
            _jobsAssigned = new ObservableCollection<Job>();
        }

        private void FillJobsGrid()
        {
            _joblist = new ObservableCollection<Job>();
            _jobsAssigned = new ObservableCollection<Job>();

            FillJobList(_joblist, @"..\..\..\jobs.csv");
            FillJobList(_jobsAssigned, @"..\..\..\donejobs.csv");

            LstJobs.ItemsSource = _joblist;
            AssignJob.ItemsSource = _joblist;
            LstJobsAssigned.ItemsSource = _jobsAssigned;
        }

        private void FillJobList(ObservableCollection<Job> jobs, string loc)
        {
            Regex csvSplit = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            var jobData = File.ReadAllLines(loc).Skip(1)
                .Select(x => csvSplit.Split(x))
                .Select(x => new Job()
                {
                    JobId = int.Parse(x[0]),
                    JobCustomer = x[1],
                    JobDate = Convert.ToDateTime(x[2]),
                    JobDuration = int.Parse(x[3]),
                    JobAddress = x[4],
                    JobModels = int.Parse(x[5]),
                    JobComments = x[6]
                });

            foreach (Job j in jobData)
            {
                jobs.Add(j);
            }
        }

        private void SaveJobs()
        {
            SaveJobList(_joblist,@"..\..\..\jobs.csv");
            SaveJobList(_jobsAssigned, @"..\..\..\donejobs.csv");
        }

        private void SaveJobList(ObservableCollection<Job> jobs, string loc)
        {
            var lines = new List<string>();
            lines.Add("Job ID,Customer,Date,Duration,Address,Models,Comments");

            foreach (Job j in jobs)
            {
                var newline = j.JobId + "," + j.JobCustomer + "," + j.JobDate.ToString("d") + "," + j.JobDuration +
                              "," + j.JobAddress + "," + j.JobModels + "," + j.JobComments;
                lines.Add(newline);
            }

            using (StreamWriter writer = new StreamWriter(loc, false))
            {
                foreach (String line in lines)
                    writer.WriteLine(line);
            }
        }

        private void SaveModels()
        {
            var lines = new List<string>();
            lines.Add("Model ID,Name,Phone,Height,Weight,Hair,Comments");

            foreach (Model m in _modellist)
            {
                var newline = m.ModelId + "," + m.ModelName + "," + m.ModelPhone + "," + m.ModelHeight +
                              "," + m.ModelWeight + "," + m.ModelHair + "," + m.ModelComments;
                lines.Add(newline);
            }

            using (StreamWriter writer = new StreamWriter(@"..\..\..\models.csv", false))
            {
                foreach (String line in lines)
                    writer.WriteLine(line);
            }
        }

        private void FillModelsGrid()
        {
            _modellist = new ObservableCollection<Model>();
            Regex csvSplit = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            var modelData = File.ReadAllLines(@"..\..\..\models.csv").Skip(1)
                .Select(x => csvSplit.Split(x))
                .Select(x => new Model()
                {
                    ModelId = int.Parse(x[0]),
                    ModelName = x[1],
                    ModelPhone = int.Parse(x[2]),
                    ModelHeight = int.Parse(x[3]),
                    ModelWeight = int.Parse(x[4]),
                    ModelHair = x[5],
                    ModelComments = x[6]
                });

            foreach (Model m in modelData)
            {
                _modellist.Add(m);
            }

            LstModels.ItemsSource = _modellist;
            AssignModel.ItemsSource = _modellist;
        }

        private void AddModelAction(object sender, RoutedEventArgs e)
        {
            if (dlg != null)
                dlg.Focus();
            else
            {
                dlg = new ModelDialog();
                dlg.Owner = this;

                dlg.Apply += new EventHandler(Dlg_Apply);
                dlg.Closed += new EventHandler(Dlg_Closed);
                dlg.Show();
 
            }
        }
        void Dlg_Closed(object sender, EventArgs e)
        {
            dlg.Apply -= new EventHandler(Dlg_Apply);
            dlg.Closed -= new EventHandler(Dlg_Closed);
            dlg = null;
            Focus();
        }

        void Dlg_Apply(object sender, EventArgs e)
        {

            ModelDialog dlg = (ModelDialog)sender;
            Model m = new Model();
            m.ModelName = dlg.Name;
            m.ModelId = _modellist.Count + 1;
            m.ModelHeight = dlg.Height;
            m.ModelHair = dlg.Hair;
            m.ModelPhone = dlg.Phone;
            m.ModelWeight = dlg.Weight;
            m.ModelComments = '"' + dlg.Comments + '"';

            _modellist.Add(m);
            LstModels.ItemsSource = _modellist;
            AssignModel.ItemsSource = _modellist;
            SaveModels();
        }


        private void AddJobAction(object sender, RoutedEventArgs e)
        {
            if (dg != null)
                dg.Focus();
            else
            {
                dg = new JobDialog();
                dg.Owner = this;

                dg.Apply += new EventHandler(Dg_Apply);
                dg.Closed += new EventHandler(Dg_Closed);
                dg.Show();

            }
        }

        void Dg_Closed(object sender, EventArgs e)
        {
            dg.Apply -= new EventHandler(Dg_Apply);
            dg.Closed -= new EventHandler(Dg_Closed);
            dg = null;
            Focus();
        }

        void Dg_Apply(object sender, EventArgs e)
        {

            JobDialog dg = (JobDialog)sender;
            Job j = new Job();
            j.JobCustomer = dg.Customer;
            j.JobId = _joblist.Count + _jobsAssigned.Count + 1;
            j.JobAddress = '"' + dg.Address + '"';
            j.JobDate = dg.Date;
            j.JobDuration = dg.Duration;
            j.JobModels = dg.Models;
            j.JobComments = '"' + dg.Comments + '"';

            _joblist.Add(j);
            LstJobs.ItemsSource = _joblist;
            AssignJob.ItemsSource = _joblist;
            SaveJobs();
        }

        private void AssignAction(object sender, RoutedEventArgs e)
        {
            var model = AssignModel.Text.Split(':');
            var job = AssignJob.Text.Split(':');
            var mid = int.Parse(model[0]);
            var jid = int.Parse(job[0]);

            foreach (Job j in _joblist)
            {
                if (j.JobId == jid)
                {
                    _jobsAssigned.Add(j);
                    _joblist.Remove(j);
                    LstJobsAssigned.ItemsSource = _jobsAssigned;
                    SaveJobs();
                    MessageBox.Show("Job: \n" + j.ToString() + "\n assigned succesfully!","Assign succesful");
                    break;
                }
            }
        }
    }
}
