﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eksamen_gui19_anita
{
    public partial class ModelDialogData
    {
        public string ModelName { get; set; }
        public int ModelPhone { get; set; }
        public int ModelHeight { get; set; }
        public int ModelWeight { get; set; }
        public string ModelHair { get; set; }
        public string ModelComment { get; set; }
    }
}
